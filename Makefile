# Copyright (C) 2018, 2019, 2020, 2021 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2018, 2019 Centre National de la Recherche Scientifique
# Copyright (C) 2018, 2019 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libhtsky.a
LIBNAME_SHARED = libhtsky.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC =\
 src/htsky_atmosphere.c\
 src/htsky.c\
 src/htsky_cloud.c\
 src/htsky_dump_cloud_vtk.c\
 src/htsky_log.c\
 src/htsky_svx.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS) $(DPDC_LIBS)

$(LIBNAME_STATIC): libhtsky.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libhtsky.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(HTCP_VERSION) htcp; then \
	  echo "htcp $(HTCP_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(HTGOP_VERSION) htgop; then \
	  echo "htgop $(HTGOP_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(HTMIE_VERSION) htmie; then \
	  echo "htmie $(HTMIE_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SVX_VERSION) svx; then \
	  echo "svx $(SVX_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS) $(DPDC_CFLAGS) -DHTSKY_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@HTCP_VERSION@#$(HTCP_VERSION)#g'\
	    -e 's#@HTGOP_VERSION@#$(HTGOP_VERSION)#g'\
	    -e 's#@HTMIE_VERSION@#$(HTMIE_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@SVX_VERSION@#$(SVX_VERSION)#g'\
	    htsky.pc.in > htsky.pc

htmie-local.pc: htmie.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@HTCP_VERSION@#$(HTCP_VERSION)#g'\
	    -e 's#@HTGOP_VERSION@#$(HTGOP_VERSION)#g'\
	    -e 's#@HTMIE_VERSION@#$(HTMIE_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@SVX_VERSION@#$(SVX_VERSION)#g'\
	    htsky.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" htsky.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/high_tune" src/htsky.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/htsky" COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/htsky.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htsky/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htsky/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/high_tune/htsky.h"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library

clean:
	rm -f $(OBJ) $(LIBNAME) .config libhtsky.o htsky.pc

distclean: clean
	rm -f $(DEP)

lint:
	shellcheck -o all make.sh
