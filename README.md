# High-Tune: Sky

This C library loads and manages the data describing a vertically
stratified atmosphere, neglecting Earth's sphericity. The physical
properties of 3D clouds are also supported.

## Requirements

- C compiler with OpenMP support
- POSIX make
- pkg-config
- [High-Tune: Cloud Properties](https://gitlab.com/meso-star/htcp)
- [High-Tune: Gas Optical Properties](https://gitlab.com/meso-star/htgop)
- [High-Tune: Mie](https://gitlab.com/meso-star/htmie)
- [RSys](https://gitlab.com/vaplv/rsys)
- [Star VoXel](https://gitlab.com/meso-star/star-vx)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.3.1

- Fix invalid memory read for clouds with irregular Z dimensions. This
  bug was triggered by clouds that were taller than they were wide, i.e.
  with the largest Z dimension. It caused a crash when accessing an
  out-of-bounds cell in the data structure used to speed up cloud
  indexing. In other situations, i.e.  when clouds were wider than they
  were high, it had no impact on calculations.

### Version 0.3

- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.2.2

- Correction of compilation errors due to API breaks in Star-VoXel 0.2.
- Correction of invalid memory writes.

### Version 0.2.1

- Fix the acceleration data structures: the Kmin and Kmax stored in the
  hierarchical trees could be wrong for cloud fields with data
  irregularly structured along the Z axis.

### Version 0.2

- Make uniform the sky setup in shortwave and in longwave. On sky
  creation, the caller now defines the type of the spectral data to
  handle (i.e.  shortwave or longwave) and their wavelength range. The
  `double wlen_lw_range[2]` member variable of the `struct htsky_args`
  data structure is thus renamed in `double wlen_range[2]` and is used
  in both cases. Finally the new member variable `enum
  htsky_spectral_type spectral_type` defines the type of this spectral
  range.
- Add the `htsky_get_raw_spectral_bounds` function that returns the
  spectral range of the loaded sky data overlapped by the user define
  wavelength range.

### Version 0.1

- Add longwave support. Add the `double wlen_lw_range[2]` member
  variable to the `struct htsky_args` data structure that, once
  correctly defined, is used to setup the sky data for the provided long
  wave range. By default this range is degenerated meaning that the sky
  is setup for the short wave range [380, 780] nm.
- Add the `htsky_find_spectral_band` function: it returns the spectral
  band that includes the submitted wavelength.
- Remove the `htsky_sample_sw_spectral_data_CIE_1931_<X|Y|Z>` functions
  that explicitly rely on the CIE XYZ color space.
- Add the
  `htsky_fetch_per_wavelength_particle_phase_function_asymmetry_parameter`
  that returns the Henyey-Greenstein phase function parameter for a
  given wavelength.

## License

Copyright (C) 2018, 2019, 2020, 2021 |Méso|Star> (contact@meso-star.com)  
Copyright (C) 2018, 2019 Centre National de la Recherche Scientifique (CNRS)  
Copyright (C) 2018, 2019 Université Paul Sabatier

HTSky is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.

