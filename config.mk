VERSION = 0.3.1
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

################################################################################
# Tools
################################################################################
AR = ar
CC = cc
LD = ld
OBJCOPY = objcopy
PKG_CONFIG = pkg-config
RANLIB = ranlib

################################################################################
# Dependencies
################################################################################
PCFLAGS_SHARED =
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

HTCP_VERSION = 0.1
HTCP_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags htcp)
HTCP_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs htcp)

HTGOP_VERSION = 0.2
HTGOP_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags htgop)
HTGOP_LIBS =  $$($(PKG_CONFIG) $(PCFLAGS) --libs htgop)

HTMIE_VERSION = 0.1
HTMIE_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags htmie)
HTMIE_LIBS =  $$($(PKG_CONFIG) $(PCFLAGS) --libs htmie)

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

SVX_VERSION = 0.3
SVX_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags svx)
SVX_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs svx)

DPDC_CFLAGS =\
 $(HTCP_CFLAGS)\
 $(HTGOP_CFLAGS)\
 $(HTMIE_CFLAGS)\
 $(RSYS_CFLAGS)\
 $(SVX_CFLAGS)\
 -fopenmp

DPDC_LIBS =\
 $(HTCP_LIBS)\
 $(HTGOP_LIBS)\
 $(HTMIE_LIBS)\
 $(RSYS_LIBS)\
 $(SVX_LIBS)\
 -fopenmp\
 -lm

################################################################################
# Compilation options
################################################################################
WFLAGS =\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_COMMON =\
 -std=c89\
 -pedantic\
 -fPIC\
 -fvisibility=hidden\
 -fstrict-aliasing\
 $(CFLAGS_HARDENED)\
 $(WFLAGS)

CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE))

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now
LDFLAGS_COMMON = -shared -Wl,--no-undefined $(LDFLAGS_HARDENED)
LDFLAGS_DEBUG = $(LDFLAGS_COMMON)
LDFLAGS_RELEASE = -s $(LDFLAGS_COMMON)
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))

OCPFLAGS_DEBUG = --localize-hidden
OCPFLAGS_RELEASE = --localize-hidden --strip-unneeded
OCPFLAGS = $(OCPFLAGS_$(BUILD_TYPE))
